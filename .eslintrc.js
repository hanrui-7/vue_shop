module.exports = {
  root: true,

  env: {
    node: true
  },

  extends: [
    'plugin:vue/essential',
    // '@vue/standard'
  ],

  parserOptions: {
    parser: 'babel-eslint'
  },

  rules: {
    'no-unused-vars': 'warn',
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/no-parsing-error': [
      2,
      {
        'x-invalid-end-tag': false
      }
    ],
    'vue/no-unused-vars': 'warn'
  },

  'extends': [
    'plugin:vue/essential'
  ]
}

